<?php 

//======================================================================
// Variables
//======================================================================


//E-mail address. Enter your email
define("__TO__", "shustenkov@mail.ru");

//Success message
define('__SUCCESS_MESSAGE__', "Ваше сообщение отправлено. Мы ответим в ближайшее время. Спасибо!");

//Error message 
define('__ERROR_MESSAGE__', "Ваше сообщение не отправлено. Пожалуйста, попробуйте еще раз.");

//Messege when one or more fields are empty
define('__MESSAGE_EMPTY_FIELDS__', "Пожалуйста, заполните все поля
");

$mail = 'test@test.test';
//======================================================================
// Do not change
//======================================================================

//E-mail validation
function check_email($email){
    if(!@eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){
        return false;
    } else {
        return true;
    }
}

//Send mail
function send_mail($to,$subject,$message,$headers){
    if(@mail($to,$subject,$message,$headers)){
        echo json_encode(array('info' => 'success', 'msg' => __SUCCESS_MESSAGE__));
    } else {
        echo json_encode(array('info' => 'error', 'msg' => __ERROR_MESSAGE__));
    }
}

//Get data form and send mail - Thank you Rafael for your contribution.
if(isset($_POST['name']) and isset($_POST['phoned'])){
	$name = $_POST['name'];
	$phone = $_POST['phoned'];

    if($name == '') {
        echo json_encode(array('info' => 'error', 'msg' => "Please enter your name."));
        exit();
    } else if($phone == ''){
        echo json_encode(array('info' => 'error', 'msg' => "Please enter valid phone."));
        exit();
    } else {
        $to = __TO__;
        $subject = $name;
        $message = '
        <html>
        <head>
          <title>Mail from '. $name .'</title>
        </head>
        <body>
          <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
            <tr style="height: 32px;">
              <th align="right" style="width:150px; padding-right:5px;">Name:</th>
              <td align="left" style="padding-left:5px; line-height: 20px;">'. $name .'</td>
            </tr>
            <tr style="height: 32px;">
              <th align="right" style="width:150px; padding-right:5px;">E-mail:</th>
              <td align="left" style="padding-left:5px; line-height: 20px;">'. $phone .'</td>
            </tr>

          </table>
        </body>
        </html>
        ';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: ' . $mail . "\r\n";

        send_mail($to,$subject,$message,$headers);
    }
} else {
    echo json_encode(array('info' => 'error', 'msg' => __MESSAGE_EMPTY_FIELDS__));
}
 ?>