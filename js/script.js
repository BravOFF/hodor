
// FIX HOME SCREEN HEIGHT
$(document).ready(function() {

    "use strict";

    setInterval(function() {

        "use strict";

        var widnowHeight = $(window).height();
        var containerHeight = $(".home-container").height();
        var padTop = widnowHeight - containerHeight;
        $(".home-container").css({
            'padding-top': Math.round(padTop / 2) + 'px',
            'padding-bottom': Math.round(padTop / 2) + 'px'
        });
    }, 10)
});

//CONTACT FORM VALIDATION
$(document).ready(function() {

    "use strict";
    
    $("#send").click(function(event) {

        event.preventDefault();

        "use strict";


        
        var name = $("#name").val();
        var phoned = $("#phone").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
        // var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if (!name) {
            $(".name_error").addClass("show").removeClass("hide");
            event.preventDefault();
        } else {
            $(".name_error").addClass("hide").removeClass("show");
        }
        if (!phoned) {
            $(".phone_error").addClass("show").removeClass("hide");
            event.preventDefault();
        } else {
            $(".phone_error").addClass("hide").removeClass("show");

        }
        console.log(name);
        console.log(phoned);

        if (name && phoned) {
            $.ajax({
                url: 'contact.php',
                dataType: 'JSON',
                data: {
                    name: name,
                    phoned: phoned,
                },
                type: 'POST',
                success: function(data) {

                    console.log(data['info']);
                    if (data['info'] == 'success') {
                        $(".Sucess").addClass("show").removeClass("hide");
                        $(".Sucess").fadeIn(2000);
                        $(".Sucess").html("<i class='fa fa-check'></i>Уважаемый <b> "+ name +" </b> Спасибо за ваш запрос, мы ответим вам как можно скорее!");
                        $("#name").val("");
                        $("#phone").val("");

                        $(".form_error .name_error, .form_error .email_error, .form_error .email_val_error, .form_error ").addClass("hide").removeClass("show");
                        $("#name").val("");
                        $("#phone").val("");
                    }else {
                        $(".Sucess").html("Уважаемый <b> "+ name +" </ b> Что-то пошло не так, попробуйте еще раз!");
                    }


                }
            });
        }
        //return false;

        event.preventDefault();
    });
});
